require 'date'
require 'github_api'
require 'bitbucket_rest_api'

bb_owner = 'username'
bb_repo = 'testinggit2bit'
bb_user = 'username'
bb_pass = 'password'

# Possible values from BitBucket
BB_STATUS = ['new','open','resolved','on hold','invalid','duplicate','wontfix']
BB_KINDS = ['bug','enhancement','proposal','task']
BB_PRIORITIES = ['trivial','minor','major','critical','blocker']

gh_repo = 'repo'
gh_user = 'username'
gh_pass = 'password'


def analyze_labels(labels,components)
	# The tag values that we are going to try to discover
	tags = { :component => nil, :priority => nil, :kind => nil, :status => nil, :labels => Array.new }
	
	labels.each do |label|
		# Store the label name
		tags[:labels].push label.name

		name = label.name.downcase
		
		# Look for a priority in available fields in BB_PRIORITIES
		if tags[:priority].nil? and BB_PRIORITIES.find_index(name)
			tags[:priority] = name
			next
		end

		# Look for a kind in available fields in BB_KINDS
		if tags[:kind].nil? and BB_KINDS.find_index(name)
			tags[:kind] = name
			next
		end

		# Look for a status in available fields in BB_STATUS
		if tags[:status].nil? and BB_STATUS.find_index(name)
			tags[:status] = name
			next
		end

		# Look for a component. Need to do case insensitive match on each known component.
		if tags[:component].nil?
			components.each do |c|
				if c.match(/#{name}/i)
					tags[:component] = c
					break
				end
			end
		end		
	end
	tags
end


# Connect to BitBucket
bitbucket = BitBucket.new :basic_auth => "#{bb_user}:#{bb_pass}",  :user => bb_user, :repo => bb_repo
# Store Milestones to search upon later on
bb_milestones = Array.new
bitbucket.issues.milestones.list(bb_owner, bb_repo).each {|m| bb_milestones.push m.name}
# Store Components to search upon later on
bb_components = Array.new
bitbucket.issues.components.list(bb_owner, bb_repo).each {|c| bb_components.push c.name}


# Setup Github connection
github = Github.new :basic_auth => "#{gh_user}:#{gh_pass}"
# Get all open and closed issues
github_data = Array.new
github_data.push github.issues.list :filter => 'all', :state => 'open', :user => gh_user, :repo => gh_repo
github_data.push github.issues.list :filter => 'all', :state => 'closed', :user => gh_user, :repo => gh_repo

# Process through each page of github issues and then through each issue per page
github_data.each do |result_set|
	result_set.each_page do |page|
	  page.each do |issue|
	
		if issue.milestone and !bb_milestones.find_index(issue.milestone.title)
			# If there is a milestone and it does not already exist, then create it
			milestone = bitbucket.issues.milestones.create bb_owner, bb_repo, { :name => issue.milestone.title }
			# Add new milestone to array so its not created again
			bb_milestones.push milestone.name
		end
		
		# Analyze the Github labels in an attempt to find the BitBucket data we need
		tags = analyze_labels issue.labels, bb_components
		
		if tags[:status].nil? or issue.state == 'closed' 
			# If a status was not defined in the github labels, then set to open/resolved
			# If the Github state is closed, we want to ignore the labels and set the ticket to resolved
			tags[:status] = issue.state == 'open' ? 'open' : 'resolved'
		end

		issue_date = DateTime.parse(issue.created_at)
		issue_body = "_Github issue **##{issue.number}** created by **#{issue.user.login}** on #{issue_date.strftime('%F %T')}_\n\n#{issue[:body]}"
		
		# Create the Bitbucket issue
		bb_issue = bitbucket.issues.create bb_owner, bb_repo, {
			:title => issue.title, 
			:content => issue_body, 
			:responsible => issue.assignee, 
			:milestone => issue.milestone ? issue.milestone.title : nil, 
			:component => tags[:component],
			:priority => tags[:priority],
			:status => tags[:status],
			:kind => tags[:kind]
		}
		
		labels = "**Github labels**: " + tags[:labels].join(', ')
		bitbucket.issues.comments.create bb_owner, bb_repo, bb_issue.local_id, {:content => labels}
		
		# Process Comments if the Github issue has any
		if issue.comments > 1
			# Get the Github comments
		    comments = github.issues.comments.all gh_user, gh_repo, issue_id: issue.number

			comments.each do |comment|
				comment_date = DateTime.parse(comment.created_at)
				content = "_Github comment by **#{comment.user.login}** on #{comment_date.strftime('%F %T')}_\n\n#{comment[:body]}"
				# Add the comment into the newly created bitbucket issue
				bitbucket.issues.comments.create bb_owner, bb_repo, bb_issue.local_id, {:content => content}
			end
		end
	  end
	exit
	end
end
